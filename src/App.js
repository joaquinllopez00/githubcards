import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { Button, Card } from "react-bootstrap";
import { GithubAPI } from "./util.js";
class App extends React.Component {
  state = {
    user: [],
    active: false,
  };
  componentDidMount = async () => {
    try {
      const result = await GithubAPI.getUser();
      this.setState({
        user: result,
      });
    } catch (err) {
      console.error(err);
    }
  };

  displayGit = () => {
    if (this.state.active === false) {
      this.setState({
        active: true,
      });
    } else {
      this.setState({
        active: false,
      });
    }
  };

  render() {
    return (
      <div className="App">
        <Button
          type="button"
          className="btn btn-primary"
          data-toggle="button"
          aria-pressed="false"
          onClick={this.displayGit}
        >
          {this.state.active === true ? "Hide Card" : "Display Card"}
        </Button>
        {this.state.active === true && (
          <Card style={{ width: "375px", textAlign: "center" }}>
            <Card.Img variant="top" src={this.state.user.avatar_url} />
            <Card.Body>
              <Card.Title>{this.state.user.name}</Card.Title>
              <Card.Text>{this.state.user.bio}</Card.Text>
            </Card.Body>
          </Card>
        )}
      </div>
    );
  }
}

export default App;
