import axios from "axios";
const getUser = async () => {
  const response = await axios.get("https://api.github.com/users/joaquinllopez00");
  return response.data;
};

export const GithubAPI = { getUser };
